const Discord = require("discord.js");

module.exports = {
  verify: function(message) {
    if (message.member == null) {
      return {
        verified: false,
        msg: `${message.author} must be a member on this server`
      };
    } else if (
      message.guild.roles
        .find(
          role => role.name === process.env.VERIFICATION_ROLE // e.g VERIFICATION_ROLE="Bot"
        )
        .comparePositionTo(message.member.highestRole) > 0
    ) {
      return {
        verified: false,
        msg: `Forbidden. Only ${
          process.env.VERIFICATION_ROLE
        } and above is authorized.`
      };
    } else {
      return {
        verified: true,
        msg: null
      };
    }
  },
  post: function(channel, description) {
    const embed = new Discord.RichEmbed()
      .setColor("#9013FE")
      .setDescription(description)
      .setTimestamp();

    channel.send({ embed });
  },
  timerArgs: [
    {
      key: "hours",
      default: -1,
      prompt: "How many hours?",
      type: "integer",
      min: 0,
      max: 100
    },
    {
      key: "minutes",
      default: 0,
      prompt: "How many minutes?",
      type: "integer",
      min: 0,
      max: 60
    },
    {
      key: "seconds",
      default: 0,
      prompt: "How many seconds?",
      type: "integer",
      min: 0,
      max: 60
    }
  ]
};

// Code snippet from https://stackoverflow.com/questions/12806304/shortest-code-to-check-if-a-number-is-in-a-range-in-javascript
Number.prototype.between = function(min, max) {
  return this >= min && this < max;
};
