const Commando = require("discord.js-commando");
const Path = require("path");
const dotenv = require("dotenv");
dotenv.config();

const ClaimManager = require("./managers/claim_manager");
const StandbyManager = require("./managers/standby_manager");
const AboutCommand = require("./commands/misc/about");

const bot = new Commando.Client({
  owner: "273597421873922048",
  commandPrefix: "c/",
  unknownCommandResponse: false
});

if (process.env.VERIFICATION_ROLE == null) {
  console.log(
    'A verification role is missing, set the environment variable VERIFICATION_ROLE="<role>"'
  );
  process.exit(9);
}

if (process.env.TOKEN != null) {
  bot.login(process.env.TOKEN);
  bot.registry
    .registerGroup("raid", "Guild Raid")
    .registerGroup("misc", "Miscellaneous")
    .registerDefaults()
    .registerCommandsIn(Path.join(__dirname, "commands"));
} else {
  console.log('Token is missing, set the environment variable TOKEN="<token>"');
  process.exit(9);
}

bot.on("error", console.error);

bot.on("ready", function() {
  ClaimManager.onReady(bot);
  StandbyManager.onReady(bot);
  AboutCommand.onReady(bot);
  bot.user.setActivity("c/help for commands", { type: "LISTENING" });
});

exports.bot = bot;
