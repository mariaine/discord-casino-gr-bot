const Util = require("../util");
const ClaimManager = require("./claim_manager");
const StandbyManager = require("./standby_manager");

var bossTimer = new Map();
var outpostTimer = new Map();

function bossTimeOut(message) {
  return function() {
    message.channel.send(
      "@here The boss has respawned, please go to the boss and use 'c/ready' to let us know when you're on standby.\nA room lead or coordinator will call out when to attack"
    );
    StandbyManager.clearPlayers(message);
  };
}

function outpostTimeOut(message) {
  return function() {
    message.channel.send("@here Outposts were reset");
    ClaimManager.clearClaims(message);
  };
}

module.exports = class TimerManager {
  /**
   * Posts the boss timer on the channel
   * @param {any} message - The message object that issued this command.
   */
  static postBossTimer(message) {
    var timer = bossTimer.get(message.channel);

    if (timer != null) {
      var timerString = TimerManager.timerToString(timer);
      Util.post(message.channel, `${timerString} left until Boss respawns`);
    } else {
      Util.post(message.channel, "Boss timer is not set.");
    }
  }

  /**
   * Sets the boss timer
   * @param {any} message - The message object that issued this command.
   * @param {number} hours - The values to set the hour of the timer to
   * @param {number} minutes - The values to set the minutes of the timer to
   * @param {number} seconds - The values to set the seconds of the timer to
   */
  static setBossTimer(message, hours, minutes, seconds) {
    var verification = Util.verify(message);

    if (verification.verified) {
      clearTimeout(bossTimer.get(message.channel));
      bossTimer.set(
        message.channel,
        setTimeout(
          bossTimeOut(message),
          (hours * 60 * 60 + minutes * 60 + seconds) * 1000
        )
      );
      Util.post(
        message.channel,
        `Boss respawn timer set to ${hours}h:${minutes}m:${seconds}s`
      );
    } else {
      Util.post(message.channel, verification.msg);
    }
  }

  /**
   * Posts the outpost timer on the channel
   * @param {any} message - The message object that issued this command.
   */
  static postOutpostTimer(message) {
    var timer = outpostTimer.get(message.channel);

    if (timer != null) {
      var timerString = this.timerToString(timer);
      Util.post(message.channel, `${timerString} left until Outpost reset`);
    } else {
      Util.post(message.channel, "Outpost timer is not set.");
    }
  }

  /**
   * Sets the outpost timer
   * @param {any} message - The message object that issued this command.
   * @param {number} hours - The values to set the hour of the timer to
   * @param {number} minutes - The values to set the minutes of the timer to
   * @param {number} seconds - The values to set the seconds of the timer to
   */
  static setOutpostTimer(message, hours, minutes, seconds) {
    var verification = Util.verify(message);

    if (verification.verified) {
      clearTimeout(outpostTimer.get(message.channel));
      outpostTimer.set(
        message.channel,
        setTimeout(
          outpostTimeOut(message),
          (hours * 60 * 60 + minutes * 60 + seconds) * 1000
        )
      );
      Util.post(
        message.channel,
        `Outpost reset timer set to ${hours}h:${minutes}m:${seconds}s`
      );
    } else {
      Util.post(message.channel, verification.msg);
    }
  }

  /**
   * Parse a time out object into a string
   * @param {*} timer - The timer object to be parsed.
   * @returns {string} A string in the "hh:mm:ss" format.
   */
  static timerToString(timer) {
    var timerSeconds = Math.ceil(
      (timer._idleStart + timer._idleTimeout - process.uptime() * 1000) / 1000
    );
    console.log(
      `Started idle at: ${timer._idleStart}\n
            Times out at: ${timer._idleTimeout}\n
            Time now:${process.uptime()}\n
            Difference: ${timerSeconds}`
    );
    var hours = Math.floor(timerSeconds / 60 / 60);
    timerSeconds -= hours * 60 * 60;
    var minutes = Math.floor(timerSeconds / 60);
    timerSeconds -= minutes * 60;
    return `${hours}h:${minutes}m:${timerSeconds}s`;
  }
};
