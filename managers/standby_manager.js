const Util = require("../util");

var readyPlayers = new Map();

module.exports = class StandbyManager {
  static onReady(bot) {
    bot.channels.forEach(channel => readyPlayers.set(channel, new Array()));
  }

  /**
   * Add the author of the command to the list of ready players
   * @param {any} message - The message object that issued this command.
   */
  static ready(message) {
    message.delete();
    var readyArray = readyPlayers.get(message.channel);

    if (!readyArray.includes(message.author)) {
      readyArray.push(message.author);
      Util.post(message.channel, `${message.author} is ready`);
    }
  }

  /**
   * Remove the author of the command from the list of ready players
   * @param {any} message - The message object that issued this command.
   */
  static unready(message) {
    message.delete();
    var readyArray = readyPlayers.get(message.channel);

    if (readyArray.includes(message.author)) {
      readyArray.splice(readyArray.indexOf(message.author), 1);
      Util.post(message.channel, `${message.author} is NOT ready`);
    }
  }

  /**
   * Post the list of ready players on the channel
   * @param {any} message - The message object that issued this command.
   */
  static show(message) {
    var readyArray = readyPlayers.get(message.channel);
    var listString = `${readyArray.length} player${
      readyArray.length == 1 ? " is" : "s are"
    } ready\n`;

    for (let player of readyArray.values()) {
      listString += `${player}\n`;
    }

    Util.post(message.channel, listString);
  }

  /**
   * Allow an authorized user to clear list of ready players
   * @param {any} message - The message object that issued this command.
   * @param {any?} user - A mentioned user [Optional]
   */
  static clearPlayers(message, user = null) {
    var verification = Util.verify(message);
    var readyArray = readyPlayers.get(message.channel);

    if (verification.verified) {
      if (user == null || user == "") {
        readyArray.splice(0, readyArray.length);
        Util.post(message.channel, "List of ready players were cleared");
      } else {
        for (let player of readyArray.values()) {
          if (player == user) {
            readyArray.splice(readyArray.indexOf(user), 1);
            Util.post(
              message.channel,
              `${message.author} was set to NOT ready`
            );
            return;
          }
        }
      }
    } else {
      Util.post(message.channel, verification.msg);
    }
  }
};
