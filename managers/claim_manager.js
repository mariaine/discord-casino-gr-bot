const Util = require("../util");

var definitions;
var raidTargets = new Map();

module.exports = class ClaimManager {
  static onReady(bot) {
    // Defines all valid inputs to c/claim <target>
    // e.g TARGETS="red;outpost a"
    if (process.env.TARGETS != null) {
      definitions = process.env.TARGETS.toLowerCase().split(";");
    } else {
      console.log(
        'Targets are missing, set the environment variable TARGETS="<target 1>;<target 2>;<...>"'
      );
      process.exit(9);
    }

    bot.channels.forEach(channel => raidTargets.set(channel, new Map()));
  }

  /**
   * Attempt to claim a path or outpost target
   * @param {any} message - The message object that issued this command.
   * @param {string} target - The target to be claimed.
   */
  static claim(message, target) {
    message.delete();
    var channelTargets = raidTargets.get(message.channel);

    if (Array.from(channelTargets.keys()).includes(target)) {
      Util.post(
        message.channel,
        `${channelTargets.get(target)} already claimed ${target}!`
      );
    } else if (Array.from(channelTargets.values()).includes(message.author)) {
      Util.post(
        message.channel,
        `${
          message.author
        } don't claim more than one target at a time!\nUse 'c/claim none' to undo previous claim`
      );
    } else if (!definitions.includes(target)) {
      Util.post(
        message.channel,
        `${
          message.author
        } Invalid target.\nUse 'c/targets' for a list of valid ones`
      );
    } else {
      channelTargets.set(target, message.author);
      Util.post(message.channel, `${message.author} claimed ${target}`);
    }
  }

  /**
   * Allow a user to clear their claim, if any
   * @param {any} message - The message object that issued this command.
   */
  static unclaim(message) {
    message.delete();
    var channelTargets = raidTargets.get(message.channel);

    for (let [target, author] of channelTargets.entries()) {
      if (author == message.author) {
        channelTargets.delete(target);
        Util.post(
          message.channel,
          `${message.author} Your claim was set to none`
        );
        return;
      }
    }
  }

  /**
   * Post a list of all currently claimed targets on the channel.
   * @param {any} message - The message object that issued this command.
   */
  static show(message) {
    var channelTargets = raidTargets.get(message.channel);
    var listString = "";

    if (channelTargets.size > 0) {
      for (let [target, author] of channelTargets.entries()) {
        listString += `**${target}**: ${author}\n`;
      }
    } else {
      listString = "List is empty";
    }

    Util.post(message.channel, listString);
  }

  /**
   * Post a list of all currently valid targets on the channel.
   */
  static showDefinitions(message) {
    var listString = "";

    if (definitions != null && definitions.length > 0) {
      for (let definition of definitions) {
        listString += `**${definition}**\n`;
      }
    } else {
      listString = "Targets are not defined";
    }

    Util.post(message.channel, listString);
  }

  /**
   * Allow an authorized user to clear all or mentioned user's claims
   * @param {any} message - The message object that issued this command.
   * @param {any?} user - A mentioned user [Optional]
   */
  static clearClaims(message, user = null) {
    var channelTargets = raidTargets.get(message.channel);
    var verification = Util.verify(message);

    if (verification.verified) {
      if (user == null || user == "") {
        channelTargets.clear();
        Util.post(message.channel, "All claims were cleared");
      } else {
        for (let [target, author] of channelTargets.entries()) {
          if (author == user) {
            channelTargets.delete(target);
            Util.post(
              message.channel,
              `${message.author} claim was set to none`
            );
            return;
          }
        }
      }
    } else {
      Util.post(message.channel, verification.msg);
    }
  }
};
