const Commando = require("discord.js-commando");
const Discord = require("discord.js");

var authors;

module.exports = class MiscAboutCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "about",
      group: "misc",
      memberName: "about",
      guildOnly: false,
      description: "Writes the about page.\nCan be used in a Direct Message",
      details: "Contains information about the author(s) and licensing"
    });
  }

  static onReady(bot) {
    authors = bot.owners;
  }

  async run(message, args) {
    const embed = new Discord.RichEmbed()
      .setColor("#9013FE")
      .setDescription(
        `This Brave Frontier guild raid bot was written by ${authors} primarily for the guild Casino's use. It is under no license and in a private repository for the time being.`
      );

    message.channel.send({ embed });
  }
};
