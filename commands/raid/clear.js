const Commando = require("discord.js-commando");
const ClaimManager = require("../../managers/claim_manager");
const StandbyManager = require("../../managers/standby_manager");

module.exports = class RaidClearCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "clear",
      group: "raid",
      memberName: "clear",
      guildOnly: true,
      description:
        "Clears a list of all users or a single user.\nOnly authorized members can use this command",
      args: [
        {
          key: "list",
          type: "string",
          prompt: "Which list to clear? (claims|standby)"
        },
        {
          key: "member",
          default: "",
          type: "user",
          prompt: "Which member to clear?"
        }
      ],
      examples: ["clear claims", "clear standby", "clear claims @member1"]
    });
  }

  async run(message, { list, member }) {
    if (list == "claims") {
      ClaimManager.clearClaims(message, member);
    } else if (list == "standby") {
      StandbyManager.clearPlayers(message, member);
    }
  }
};
