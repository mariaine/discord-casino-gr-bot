const Commando = require("discord.js-commando");
const ClaimManager = require("../../managers/claim_manager");

module.exports = class RaidClaimsCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "claims",
      group: "raid",
      memberName: "claims",
      guildOnly: true,
      description: "List all current claims"
    });
  }

  async run(message) {
    ClaimManager.show(message);
  }
};
