const Commando = require("discord.js-commando");
const ClaimManager = require("../../managers/claim_manager");

module.exports = class RaidClaimCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "claim",
      group: "raid",
      memberName: "claim",
      guildOnly: true,
      description: "Claim a target, such as a path or an outpost.",
      details:
        "Lay claim to a valid path or outpost as defined by 'c/targets'.\nReset your claim with 'c/claim none'",
      args: [
        {
          key: "target",
          type: "string",
          prompt: "Which target would you like to claim?"
        }
      ],
      examples: ["claim outpost a", "claim none", "claim blue"]
    });
  }

  async run(message, { target }) {
    if (target != "none") {
      ClaimManager.claim(message, target);
    } else {
      ClaimManager.unclaim(message);
    }
  }
};
