const Commando = require("discord.js-commando");
const Util = require("../../util");
const TimerManager = require("../../managers/timer_manager");

module.exports = class RaidOutpostCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "outpost",
      group: "raid",
      memberName: "outpost",
      guildOnly: true,
      description: "Gets or sets the current outpost timer.",
      args: Util.timerArgs,
      examples: ["outpost", "outpost 3", "outpost 0 35 17"]
    });
  }

  async run(message, { hours, minutes, seconds }) {
    if (hours < 0) {
      TimerManager.postOutpostTimer(message);
    } else {
      TimerManager.setOutpostTimer(message, hours, minutes, seconds);
    }
  }
};
