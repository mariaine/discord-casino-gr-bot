const Commando = require("discord.js-commando");
const ClaimManager = require("../../managers/claim_manager");

module.exports = class RaidTargetsCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "targets",
      group: "raid",
      memberName: "targets",
      guildOnly: false,
      description: "List all valid targets.\nCan be used in a Direct Message"
    });
  }

  async run(message) {
    ClaimManager.showDefinitions(message);
  }
};
