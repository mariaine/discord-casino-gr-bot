const Commando = require("discord.js-commando");
const Util = require("../../util");
const TimerManager = require("../../managers/timer_manager");

module.exports = class RaidBossCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "boss",
      group: "raid",
      memberName: "boss",
      guildOnly: true,
      description: "Gets or sets the current boss respawn timer.",
      args: Util.timerArgs,
      examples: ["boss", "boss 1 30", "boss 1 27 9", "boss 1h 25m 7s"]
    });
  }

  async run(message, { hours, minutes, seconds }) {
    if (hours < 0) {
      TimerManager.postBossTimer(message);
    } else {
      TimerManager.setBossTimer(message, hours, minutes, seconds);
    }
  }
};
