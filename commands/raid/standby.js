const Commando = require("discord.js-commando");
const StandbyManager = require("../../managers/standby_manager");

module.exports = class RaidStandbyCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "standby",
      group: "raid",
      memberName: "standby",
      guildOnly: true,
      description: "List everyone on standby to attack the boss."
    });
  }

  async run(message) {
    StandbyManager.show(message);
  }
};
