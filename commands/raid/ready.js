const Commando = require("discord.js-commando");
const StandbyManager = require("../../managers/standby_manager");

module.exports = class RaidReadyCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "ready",
      group: "raid",
      memberName: "ready",
      guildOnly: true,
      description: "Sets you on standby to attack the boss.",
      args: [{ key: "option", default: "", prompt: "", type: "string" }],
      examples: ["ready", "ready not"]
    });
  }

  async run(message, { option }) {
    if (!option) {
      StandbyManager.ready(message);
    } else if (option == "not") {
      StandbyManager.unready(message);
    }
  }
};
